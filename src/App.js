import React, {Component, useState} from 'react';
import Buttons from "./Component/ModalButtons";
import ModalWindow from "./Component/ModalWindow";
import "./App.css"


function App() {
  const  [modalOne,firstStatus]=useState(false);
  const  [modalTwo,secondStatus]=useState(false);
  const toggleModalOne =()=>firstStatus(e => !e);
  const toggleModalTwo =()=>secondStatus(j => !j);
  return (
      <div className="ModalApp">
        <Buttons backgroundColor="black" text="Open First Modal Window" onClick={toggleModalOne}/>
        <Buttons backgroundColor="blue" text="Open Second Modal Window" onClick={toggleModalTwo}/>
        {modalOne && (
            <ModalWindow header="Do you want to delete this file?"
                         closeicon={true}
                         text="Once you delete this file, it won't be possible to undo this action.
Are you sure you want to delete it?"
                         close={toggleModalOne}
                         actions={[
                           <Buttons
                               key={1}
                               backgroundColor="red"
                               text="Ok"
                               onClick={toggleModalOne}/>,
                           <Buttons
                               key={1}
                               backgroundColor="red"
                               text="Cancel"
                               onClick={toggleModalOne}/>
                         ]}/>)}
        {modalTwo &&
        (
            <ModalWindow header="Do you want to delete this file?"
                         closeicon={true}
                         text="Once you delete this file, it won't be possible to undo this action.
Are you sure you want to delete it?"
                         close={toggleModalTwo}
                         actions={[
                           <Buttons
                               key={1}
                               backgroundColor="red"
                               text="Ok"
                               onClick={toggleModalTwo}/>,
                           <Buttons
                               key={1}
                               backgroundColor="red"
                               text="Cancel"
                               onClick={toggleModalTwo}/>
                         ]}/>)
        }
      </div>
  );
}
export default App;
