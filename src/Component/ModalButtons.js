import React, {Component} from 'react';
import PropTypes from 'prop-types';


const ModalButtons = ({text, backgroundColor,onClick})=> {
    return (
        <button className="button" style={{backgroundColor}} onClick={onClick}></button>
    );
    ModalButtons.propTypes ={
        text: PropTypes.string,
        backgroundColor: PropTypes.string,
        onClick: PropTypes.func
    }
}
export default ModalButtons;