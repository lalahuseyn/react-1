import React, {Component} from 'react';
import PropTypes from 'prop-types';
const ModalWindow =({header,closeicon, actions, text,close})=> {
    return (
        <div className="opened-modal">
            <header>
                {header}
                {closeicon && <button onClick={close} className="close">X</button>}
            </header>
            <p className="opened-modal-text">{text}</p>
            {actions}
        </div>
    );
};
export default ModalWindow;